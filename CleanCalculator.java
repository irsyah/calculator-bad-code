import java.util.*;

class CleanCalculator {
    public static void main(String args[]) {

        String mathProblems = getInputFromUser();
        List<String> numberAndOperators= parsingMathProblemsToList(mathProblems); //[ 5, + , 10, * , 2]

        //perhitungan
        double total = 0;

        for (int i = 0; i < numberAndOperators.size(); i++) {
            switch (numberAndOperators.get(i)) {
                //i++ in case for skip 1 process
                case "+":
                    i++;
                    total = add(total, Double.parseDouble(numberAndOperators.get(i)));
                    break;
                case "-":
                    i++;
                    total = subtract(total, Double.parseDouble(numberAndOperators.get(i)));
                    break;
                case "/":
                    i++;
                    total = divide(total, Double.parseDouble(numberAndOperators.get(i)));
                    break;
                case "*":
                    i++;
                    total = multiple(total, Double.parseDouble(numberAndOperators.get(i)));
                    break;
                default:
                    total = Double.parseDouble(numberAndOperators.get(i));
            }
        }

        print("** Result:" + total);
    }

    public static String getInputFromUser() {
        Scanner inputScanner = new Scanner(System.in);
        //example input: 5+10*2
        print("Input number and operators that you want to calculate: ");
        String mathProblems = "";

        try {
            mathProblems = inputScanner.next(); //15+10*2
        } catch (InputMismatchException e) {
            print("Invalid input");
        }

        return mathProblems;
    }

    public static List<String> parsingMathProblemsToList(String mathProblems) {
        List<String> numberAndOperators = new ArrayList<>();
        String tempInputScanner = "";

        for (int i = 0; i < mathProblems.length(); i++) {
            if(mathProblems.charAt(i) == '+' || mathProblems.charAt(i) == '-' || mathProblems.charAt(i) == '/') {
                numberAndOperators.add(tempInputScanner);
                numberAndOperators.add(String.valueOf(mathProblems.charAt(i)));

                tempInputScanner = "";
            } else {
                tempInputScanner = tempInputScanner + String.valueOf(mathProblems.charAt(i));
            }

            if (i == mathProblems.length() - 1) {
                numberAndOperators.add(tempInputScanner);
                tempInputScanner = "";
            }
        }

        return numberAndOperators;
    }

    public static double add(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    public static double subtract(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }

    public static double divide(double firstNumber, double secondNumber) {
        return firstNumber / secondNumber;
    }

    public static double multiple(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    public static void print(String message) {
        System.out.println(message);
    }
}
